module TokenAuth
  module RedisAdapter
    extend ActiveSupport::Concern

    class_methods do
      def get_hash(uid)
        ::TokenAuth::redis.hgetall(build_redis_key(uid)).symbolize_keys
      end

      def delete_key(uid)
        ::TokenAuth::redis.del(build_redis_key(uid))
      end

      def set_hash(uid, args = {})
        expiration = args.delete(:expiration)
        key = build_redis_key(uid)

        ::TokenAuth::redis.mapped_hmset(key, args)
        ::TokenAuth::redis.expire(key, expiration.to_i) if expiration.present?
      end

      def expire_key(uid, expiration)
        ::TokenAuth::redis.expire(build_redis_key(uid), expiration)
      end

      def key_exists(uid)
        ::TokenAuth::redis.exists?(build_redis_key(uid))
      end

      def build_redis_key(uid)
        ['token_auth', self.name.parameterize.singularize.underscore, uid].join(':')
      end
    end
  end
end
