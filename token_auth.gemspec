$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "token_auth/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "token_auth"
  s.version     = TokenAuth::VERSION
  s.authors     = ["Anton"]
  s.email       = ["amdj15@gmail.com"]
  s.homepage    = "https://github.com/amdj15"
  s.summary     = "Summary of TokenAuth."
  s.description = "Description of TokenAuth."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["spec/**/*"]

  s.add_dependency "rails", ">= 6.1.0", "< 8"
  s.add_dependency "redis", "~> 4.2"

  s.add_development_dependency 'rspec-rails', "~> 3.8.1"
  s.add_development_dependency 'faker', "~> 3.0"
end
